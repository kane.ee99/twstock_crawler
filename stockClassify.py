import requests
import pandas as pd
import utils


def crawl():
    
    # 上市
    url = 'https://isin.twse.com.tw/isin/C_public.jsp?strMode=2'
    # 上櫃
    url = 'https://isin.twse.com.tw/isin/C_public.jsp?strMode=4'
    
    r = requests.get(url)
    r.encoding = 'big5'
    
    dfs = pd.read_html(r.text, header=None)
    df = dfs[0]
    df.columns = df.iloc[0,:]
    
    res = df[~df['產業別'].isnull()]
    res = res[res['CFICode']=='ESVUFR']
    
    res[['公司代號','公司名稱']] = res['有價證券代號及名稱'].str.split(expand=True)
    res = res[['公司代號','公司名稱','市場別','產業別','上市日']]
    
    utils.insert_value('stock', res)
