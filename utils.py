import time
from functools import wraps
from config import CONN
import pandas as pd
import datetime

def execute_timer(func):
    @wraps(func)
    def wrapper(*arg, **kwarg):
        t1 = time.time()
        func(*arg, **kwarg)
        t2 = time.time()
        func_name = func.__name__
        execute_time = round(t2-t1, 2)
        print(f'{func_name} : {execute_time} s')
    return wrapper


def create_table(table_name, df):
    columns = list(df.columns)
    sqlstr = f'create table {table_name} ("'
    sqlstr += '" real,"'.join(columns)
    sqlstr += '" real)'
    cursor = CONN.cursor()
    cursor.execute(sqlstr)
    CONN.commit()
    cursor.close()
    
    
def insert_value(table_name, df):
    df_col = list(df.columns)    
    sqlstr = f'insert into {table_name} '
    sqlstr += '([' + '],['.join(df_col) + ']) values('
    sqlstr += ','.join(['?']*len(df.columns)) + ')'
    cursor = CONN.cursor()
    for index, row in df.iterrows():
        try:
            value = list(row)
            cursor.execute(sqlstr, value)
        except Exception as err:
            print(str(err))
        CONN.commit()
    cursor.close()


def delete_income_statement(year, season):
    sqlstr = f'delete from income_statement where 年度={year} and 季={season}'
    cursor = CONN.cursor()
    cursor.execute(sqlstr)
    CONN.commit()
    cursor.close()
    

def delete_month_revenue(year, month):
    sqlstr = f'delete from month_revenue where 年={year} and 月={month}'
    cursor = CONN.cursor()
    cursor.execute(sqlstr)
    CONN.commit()
    cursor.close()
    

def alter_col(table_name, add_col):
    if len(add_col)!= 0:
        cursor = CONN.cursor()
        for col in add_col:
            sqlstr = f'''alter table {table_name} add [{col}] real'''
            cursor.execute(sqlstr)
            CONN.commit()
        cursor.close()
            
            
def get_add_col(table_name, df):
    table_col = get_table_col(table_name)
    df_col = get_df_col(df)
    add_col = list(df_col-table_col)
    return add_col


def get_table_col(table_name):
    sqlstr = f'''PRAGMA table_info('{table_name}')'''
    table_info = pd.read_sql(sqlstr, CONN)
    table_col = set(table_info['name'])
    return table_col


def get_df_col(df):
    df_col = set(df.columns)
    return df_col


def get_year_season():
    today = datetime.datetime.today()
    year = today.year
    year_ch = year-1911
    q1 = datetime.datetime(year,5,15)
    q2 = datetime.datetime(year,8,14)
    q3 = datetime.datetime(year,11,14)
    q4 = datetime.datetime(year+1,3,31)
    q5 = datetime.datetime(year,3,31)
    if q1<=today<q2:
        return year_ch, 1
    if q2<=today<q3:
        return year_ch, 2
    if q3<=today<q4:
        return year_ch, 3
    if today<q5:
        return year_ch-1, 3
    if q5<=today<q1:
        return year_ch-1, 4
    
def get_year_month():
    today = datetime.datetime.today()
    year = today.year
    year_ch = year-1911
    month = today.month
    day = today.day
    
    if day < 10:
        if month == 1:
            month = 12
        else:
            month -= 1

    return year_ch, month
    

def get_DB_year_season_income():
    sqlstr = '''
    SELECT 年度, 季 FROM income_statement
    ORDER BY 年度 DESC, 季 desc
    LIMIT 1
    '''
    df = pd.read_sql(sqlstr, CONN)
    year = df.iloc[0,0]
    season = df.iloc[0,1]
    
    return year, season

def get_DB_year_month_revenue():
    sqlstr = '''
    select 年, 月 from month_revenue
    order by 年 desc, 月 DESC
    limit 1
    '''
    df = pd.read_sql(sqlstr, CONN)
    year = df.iloc[0,0]
    month = df.iloc[0,1]
    
    return year, month


