from config import INCOME_STATEMENT_PK, MONTH_REVENUE_COL
from config import MONTH_REVENUE_COMBINE_COL, MONTH_REVENUE_COMBINE_COL_NAME
from utils import execute_timer, get_add_col, alter_col, insert_value
import pandas as pd
import numpy as np
import requests
import time



def income_statement(year, season, s_type='sii'):
    url = 'https://mops.twse.com.tw/mops/web/t163sb04'
    param = {'encodeURIComponent':1,
             'step':1,
             'firstin':1,
             'off':1,
             'TYPEK':s_type,
             'year':str(year),
             'season':str(season)}
    r = requests.post(url, data=param)
    r.encoding = 'utf-8'
    dfs = pd.read_html(r.text, header=None)
    df = pd.concat(dfs[10:], axis=0, sort=True)
    df = df.replace('--', np.nan)
    df['類型'] = '綜合'
    df['年度'] = int(year)
    df['季'] = int(season)
    df['公司代號'] = df['公司代號'].astype(int).astype(str)
    df = df.dropna(axis=1, how='all')
    pk_col = INCOME_STATEMENT_PK
    not_pk_col = list(set(df.columns)-set(INCOME_STATEMENT_PK))
    df = df[pk_col + not_pk_col]
    df.iloc[:,len(pk_col):] = df.iloc[:,len(pk_col):].astype(float)

    return df



def income_statement_signle(year, season):
    url = 'https://mops.twse.com.tw/mops/web/t51sb08'
    param = {'encodeURIComponent':1,
             'step':1,
             'firstin':1,
             'off':1,
             'TYPEK':'sii',
             'year':str(year),
             'season':'0'+str(season)}
    r = requests.post(url, data=param)
    r.encoding = 'utf-8'
    dfs = pd.read_html(r.text, header=None)
    df = pd.concat(dfs[10:], axis=0, sort=True)
    df = df.replace('--', np.nan)
    df['類型'] = '非合併'
    df['年度'] = int(year)
    df['季'] = int(season)
    df = df.dropna(axis=1, how='all')
    pk_col = INCOME_STATEMENT_PK
    not_pk_col = list(set(df.columns)-set(INCOME_STATEMENT_PK))
    df = df[pk_col + not_pk_col]
    df = df[df['公司名稱']!='公司名稱']
    df.iloc[:,len(pk_col):] = df.iloc[:,len(pk_col):].apply(pd.to_numeric)

    return df



def income_statement_combine(year, season):
    url = 'https://mops.twse.com.tw/mops/web/t51sb13'
    param = {'encodeURIComponent':1,
             'step':1,
             'firstin':1,
             'off':1,
             'TYPEK':'otc',
             'year':str(year),
             'season':'0'+str(season)}
    r = requests.post(url, data=param)
    r.encoding = 'utf-8'
    dfs = pd.read_html(r.text, header=None)
    for df in dfs:
        if type(df.columns[0]) is tuple:
            df.columns = [x[1] for x in list(df.columns)]
    df = pd.concat(dfs[10:], axis=0, sort=True)
    df = df.replace('--', np.nan)
    df['類型'] = '合併'
    df['年度'] = int(year)
    df['季'] = int(season)
    df = df.dropna(axis=1, how='all')
    pk_col = INCOME_STATEMENT_PK
    not_pk_col = list(set(df.columns)-set(INCOME_STATEMENT_PK))
    df = df[pk_col + not_pk_col]
    df = df[df['公司名稱']!='公司名稱']
    df = df[~df['公司名稱'].isnull()]
    df = df[df.columns[~df.columns.isin([0,'換算匯率參考依據','換算匯率'])]]
    df.iloc[:,len(pk_col):] = df.iloc[:,len(pk_col):].apply(pd.to_numeric)

    return df


def month_revenue(year, month, data_type='sii'):
    url = f'https://mops.twse.com.tw/nas/t21/{data_type}/t21sc03_{year}_{month}_0.html'
    #elif data_type=='非合併':
    #    url = f'https://mops.twse.com.tw/nas/t21/sii/t21sc03_{year}_{month}.html'
        
    r = requests.get(url)
    r.encoding = 'big5-hkscs'
    dfs = pd.read_html(r.text)
    for df in dfs:
        if type(df.columns[0]) is tuple:
            df.columns = [x[1] for x in list(df.columns)]
    df = pd.concat(dfs, axis=0, sort=True)
    df = df.dropna(subset=['公司代號'])
    df = df[df['公司代號'].str.isdigit()]
    df['類型'] = 'ifrs'
    df['年'] = int(year)
    df['月'] = int(month)
    df['公司代號'] = df['公司代號'].astype(int).astype(str)
    col = set(df.columns).intersection(set(MONTH_REVENUE_COL))
    df = df[col]
    return df


def month_revenue_combine(year, month):
    url = f'https://mops.twse.com.tw/mops/web/t21sb06'
    if month>=10: month_str = str(month)
    else: month_str = '0'+str(month)
    param = {'encodeURIComponent':1,
             'step':1,
             'firstin':1,
             'off':1,
             'TYPEK':'otc',
             'year':str(year),
             'month':month_str}
    r = requests.post(url, param)
    r.encoding = 'utf-8'
    dfs = pd.read_html(r.text, header=None)
    for df in dfs:
        if type(df.columns[0]) is tuple:
            df.columns = [x[1] for x in list(df.columns)]
    df = pd.concat(dfs, axis=0, sort=True)
    df = df.dropna(subset=['公司代號'])
    df['類型'] = '合併'
    df['年'] = int(year)
    df['月'] = int(month)
    df['公司代號'] = df['公司代號'].astype(int).astype(str)
    df = df[MONTH_REVENUE_COMBINE_COL]
    df.columns = MONTH_REVENUE_COMBINE_COL_NAME
    return df
    