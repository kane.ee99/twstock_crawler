from utils import execute_timer, get_add_col, alter_col, insert_value
from utils import delete_income_statement, get_year_season, get_DB_year_season_income
from crawler import income_statement
import datetime
import time


@execute_timer
def crawl_insert(year, season, data_type='sii'):
    table_name = 'income_statement'
    df = income_statement(year, season, data_type)
    add_col = get_add_col(table_name, df)
    alter_col(table_name, add_col)
    insert_value(table_name, df)
    print(f'Successfully crawl and insert {data_type}.')
    

def main():
    Eyear, Eseason = get_year_season()
    Syear, Sseason = get_DB_year_season_income()
    
    Sys = 4 * Syear + Sseason
    Eys = 4 * Eyear + Eseason
    
    for ys in range(Sys,Eys):
        year,season = divmod(ys,4)
        season += 1

        delete_income_statement(year, season)
        crawl_insert(year, season,'sii')
        time.sleep(10)
        crawl_insert(year, season,'otc')
    
    
if __name__ == '__main__':
    main()