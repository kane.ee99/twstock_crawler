from utils import execute_timer, get_add_col, alter_col, insert_value
from utils import delete_month_revenue, get_DB_year_month_revenue, get_year_month
from crawler import month_revenue
import datetime
import time


@execute_timer
def crawl_insert(year, month, data_type='sii'):
    table_name = 'month_revenue'
    df = month_revenue(year, month, data_type)
    add_col = get_add_col(table_name, df)
    alter_col(table_name, add_col)
    insert_value(table_name, df)
    print(f'Successfully crawl and insert {year} {month} {data_type}.')
    

def main():
    Eyear, Emonth = get_year_month()
    Syear, Smonth = get_DB_year_month_revenue()
    
    Sys = 12 * Syear + Smonth
    Eys = 12 * Eyear + Emonth
    
    for ys in range(Sys,Eys):
        year,month = divmod(ys, 12)
        month += 1

        delete_month_revenue(year, month)
        crawl_insert(year, month, 'sii')
        time.sleep(10)
        crawl_insert(year, month, 'otc')
    
    
    
if __name__ == '__main__':
    main()